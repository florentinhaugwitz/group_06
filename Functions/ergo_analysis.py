"""
An analyis of the worldwide energy consumption and emissions from 1970 to 2016.
The goal is to analyze the data in multiple charts and perspectives.
"""
import os
from typing import Union
import warnings
import pandas as pd
import numpy as np
import altair as alt
from statsmodels.tsa.arima.model import ARIMA

warnings.filterwarnings("ignore")


class ErgoAnalysis:
    """
    An analyis of the worldwide energy consumption since 1970 until 2016.
    The goal is to display the data in various charts and
    to filter on individual countries or show a comparison between countries.

    Parameters
    ---------------
    df_url: string
        URL to locate the data if not downloaded yet
    df_energy: pd.DataFrame
        Dataframe for storing and analyzin data
    consumption_cols: list
        List of all relevant columns for consumption of energy
    """

    def __init__(self, df_url: str):
        self.df_url = df_url
        self.df_energy = pd.DataFrame()
        self.consumption_cols = [
            "biofuel_consumption",
            "coal_consumption",
            "gas_consumption",
            "hydro_consumption",
            "nuclear_consumption",
            "oil_consumption",
            "other_renewable_consumption",
            "solar_consumption",
            "wind_consumption",
        ]

    def download_data_file(self, download_file: str) -> None:
        """
        Downloads the data file into a downloads/ directory in the root
        directory of the project (main project directory).
        If the data file already exists, which is found with the attribute df_url,
        the method will not download it again.
        Reads the dataset into a pandas dataframe which is an attribute of the class
        and only considers years after 1970, inclusively.

        Parameters
        ---------------
        download_file: str
            URL of the file that should be downloaded
            if it is not already in the downloads folder

        Returns
        ---------------
        None

        """

        try:
            df_energy = pd.read_csv(f"../downloads/{self.df_url}")

        # If data is not in folder then we will download it from github
        except FileNotFoundError:
            df_energy = pd.read_csv(download_file)

            # Create a new directory if it does not exist
            if not os.path.exists("../downloads/"):
                os.makedirs("../downloads/")
            df_energy.to_csv(f"../downloads/{self.df_url}", index=False)

        finally:
            # Years after 1970 and until 2016
            self.df_energy = df_energy[(df_energy["year"] >= 1970) &
                                       (df_energy.year <= 2016)].copy()

            # Calculation of emission column
            table_values = [1450, 1000, 455, 90, 5.5, 1200, 0, 53, 14]
            division = (1 * 10**9) / (1 * 10**6)
            self.df_energy["emissions"] = self.df_energy.apply(
                lambda x: np.nansum(
                    (x[self.consumption_cols] * division) * table_values
                ),
                axis=1,
            )

            self.df_energy["year"] = pd.to_datetime(self.df_energy["year"], format="%Y")
            self.df_energy.set_index("year", inplace=True)

    def unique_values(self, column: str = "country", index: bool = False) -> np.ndarray:
        """
        Returns all the unique values in a specific column.

        Parameters
        ---------------
        column: string
            Name of the column to be used.

        index: boolean
            Used to query the index of the dataframe. Is by default False.

        Returns
        ---------------
        final_list: np.ndarray
            Unique numpy array of elements present in column "column"
            of the attribute of the class called "df_energy".

        """
        # Raise Error if input of user is incorrect
        if not isinstance(column, str):
            raise TypeError("Only strings are allowed as column names")
        if not isinstance(index, bool):
            raise TypeError("Only True or False is allowed for the index value")
        if column not in self.df_energy.columns:
            raise ValueError(f"Column {column} is not in the data set")

        # Check if index or a column should be returned
        if index:
            values = self.df_energy.index.unique()
        else:
            values = self.df_energy[column].unique()

        return values

    def consumption_sources(
        self, country: str, normalize: bool = True
    ) -> alt.vegalite.v4.api.Chart:
        """
        Returns a normalized stacked area chart of the "_consumption" columns for a given country.

        Parameters
        ---------------
        country: string
            Country from wich we want to retrieve information.
        normalize: boolean
            Either True if plot to be normalized (%) or False if not to be normalized.

        Returns
        ---------------
        chart: alt.vegalite.v4.api.Chart
            Stacked area chart with the year and total consumption for the selected country.

        """
        # Raise Error if input of user is incorrect
        if not isinstance(country, str):
            raise TypeError("Only strings are allowed as country names")
        if not isinstance(normalize, bool):
            raise TypeError("Only True or False is allowed")
        if country not in self.unique_values():
            raise ValueError(f"Country {country} is not in the data set")

        # Select only consumption columns and year
        country_df = self.df_energy[(self.df_energy["country"] == country)].copy()

        # Turn dataframe from wide to long format
        country_consumption_df = country_df[self.consumption_cols].melt(
            var_name="source", value_name="consumption", ignore_index=False
        )

        # Normalize chart or not
        normalization = "normalize" if normalize else "zero"

        # Get selection of columns for interactive legend of chart
        selection = alt.selection_multi(fields=["source"], bind="legend")

        # Creation of chart with altair
        chart = (
            alt.Chart(country_consumption_df.reset_index())
            .mark_area()
            .encode(
                x=alt.X("year:T", axis=alt.Axis(title="Year")),
                y=alt.Y(
                    "consumption:Q",
                    stack=normalization,
                    axis=alt.Axis(
                        title=f"{'Share' if normalize else 'Amount'} of consumption"
                    ),
                ),
                color="source:N",
                opacity=alt.condition(
                    selection, alt.value(1), alt.value(0.2)
                ),  # Changes opacity if source is selected in legend
                tooltip=[
                    alt.Tooltip(title="Source", field="source"),
                    alt.Tooltip(title="Year", field="year", timeUnit="year"),
                    alt.Tooltip(title="Consumption (THh)", field="consumption"),
                ],
            )
            .properties(
                title=f"Energy Consumption for {country}", width=800, height=300
            )
            .add_selection(selection)
        )

        return chart

    def total_consumption_comparison(
        self, country_selection: Union[str, list[str]]
    ) -> alt.vegalite.v4.api.LayerChart:
        """
        Plots the total consumption of one or multiple countries in a line plot.
        All columns with from the attribute consumption_cols will be added
        up into one number per year and country.

        Parameters
        ---------------
        country_selection: Union[str, list[str]]
            This can either be a single country or a list of countries that will compared

        Returns
        ---------------
        chart: alt.vegalite.v4.api.LayerChart
            Mutliple line chart with the year, total consumption
            and total emission to compare the selected country or countries.
            The total emissions are displayed as straight lines and the emissions as dashed lines

        """
        # Reduce dataframe to selected country/countries
        # Title is for the title in the altair chart
        if isinstance(country_selection, str):

            # Raise Error if input of user is incorrect
            if country_selection not in self.unique_values():
                raise ValueError(f"Country {country_selection} is not in the data set")

            # If country is in data set, filter data set to the country
            country_df = self.df_energy[self.df_energy["country"] == country_selection].copy()
            title = country_selection
        elif (
            bool(country_selection)
            and isinstance(country_selection, list)
            and all(isinstance(elem, str) for elem in country_selection)
        ):

            # Raise Error if input of user is incorrect
            if not any(elem in self.unique_values() for elem in country_selection):
                raise ValueError(
                    f"None of the countries {country_selection} are in the data set"
                )

            # If at least one country is in data set,
            # Then filter data set to the that/those country/countries
            country_df = self.df_energy[self.df_energy["country"].isin(country_selection)].copy()
            title = f"{' vs. '.join(country_selection)}"
        else:
            raise TypeError(
                "Country selection must either be a string or a list of strings"
            )

        # Create total consumption based on all columns ending with "_consumption"
        # and round it by the last 3 digits after comma
        country_df["total_consumption"] = (
            country_df[self.consumption_cols].sum(axis=1).round(3)
        )

        # Creation of chart with altair
        # Base chart for x axis
        base = (
            alt.Chart(country_df.reset_index())
            .mark_line()
            .encode(
                x=alt.X("year:T", axis=alt.Axis(title="Year")),
                tooltip=[
                    alt.Tooltip(title="Country", field="country"),
                    alt.Tooltip(title="Year", field="year", timeUnit="year"),
                    alt.Tooltip(title="Consumption in TWh", field="total_consumption"),
                ],
            )
        ).properties(title=f"Total consumption for {title}", width=800, height=300)

        # Consumption line
        consumption = base.mark_line().encode(
            y=alt.Y(
                "total_consumption:Q",
                axis=alt.Axis(
                    title="Amount of Consumption",
                    labelExpr="datum.value + 'TWh'",
                ),
            ),
            color="country:N",
        )

        # Emission line
        emission = base.mark_line(strokeDash=[3, 5]).encode(
            alt.Y(
                "emissions:Q",
                axis=alt.Axis(title="Emissions", labelExpr="datum.value + 't/TWh'"),
            ),
            color="country:N",
        )

        return alt.layer(consumption, emission).resolve_scale(y="independent")

    def gdp_comparison(
        self, country_selection: Union[str, list[str]]
    ) -> alt.vegalite.v4.api.Chart:
        """
        Plots the GDP of one or multiple countries in a line plot.

        Parameters
        ---------------
        country_selection: Union[str, list[str]]
            This can either be a single country or a list of countries that will compared

        Returns
        ---------------
        chart: alt.vegalite.v4.api.Chart
            Mutliple line chart with the year and GDP to compare the selected country or countries

        """
        # Reduce dataframe to selected country/countries
        # Title is for the title in the altair chart
        if isinstance(country_selection, str):

            # Raise Error if input of user is incorrect
            if country_selection not in self.unique_values():
                raise ValueError(f"Country {country_selection} is not in the data set")

            # If country is in data set, filter data set to the country and only get country and gdp
            gdp_df = self.df_energy[self.df_energy["country"] == country_selection][
                ["country", "gdp"]
            ].copy()
            title = country_selection

        # Check if all elements in list are of type str
        elif (
            bool(country_selection)
            and isinstance(country_selection, list)
            and all(isinstance(elem, str) for elem in country_selection)
        ):
            # Raise Error if input of user is incorrect
            if not any(elem in self.unique_values() for elem in country_selection):
                raise ValueError(
                    f"None of the countries {country_selection} are in the data set"
                )

            # If at least one country is in data set, filter data set to
            # that/those country/countries and only get country and gdp
            gdp_df = self.df_energy[self.df_energy["country"].isin(country_selection)][
                ["country", "gdp"]
            ].copy()
            title = f"{' vs. '.join(country_selection)}"
        else:
            raise TypeError(
                "Country selection must either be a string or a list of strings"
            )

        # Get selection of columns for interactive legend of chart
        selection = alt.selection_multi(fields=["country"], bind="legend")

        # Creation of chart with altair
        chart = (
            alt.Chart(gdp_df.reset_index())
            .mark_line()
            .encode(
                x=alt.X("year:T", axis=alt.Axis(title="Year")),
                y=alt.Y(
                    "gdp:Q",
                    axis=alt.Axis(
                        title="GDP value",
                        labelExpr="'$' + datum.value / 1E12 + 'T'",
                    ),
                ),
                color="country:N",
                opacity=alt.condition(
                    selection, alt.value(1), alt.value(0.2)
                ),  # Changes opacity if source is selected in legend
                tooltip=[
                    alt.Tooltip(title="Country", field="country"),
                    alt.Tooltip(title="Year", field="year", timeUnit="year"),
                    alt.Tooltip(title="GDP", field="gdp"),
                ],
            )
            .properties(title=f"GDP of {title}", width=800, height=300)
            .add_selection(selection)
        )

        return chart

    def gapminder(
        self, year: int, with_world: bool = True
    ) -> alt.vegalite.v4.api.Chart:
        """
        Plots the GDP and total energy consumption of all countries for a specific year.

        Parameters
        ---------------
        year: int
            The year to plot the chart
        with_world: bool
            Have the world as a country in the dataframe or not

        Returns
        ---------------
        chart: alt.vegalite.v4.api.Chart
            Scatter plot of all countries with the GDP as the x-axis
            and the total energy consumption as the y-axis.
            The sizes are based on the population size.

        """

        # In case someone enters something else than a string or a list
        if not isinstance(year, int):
            raise TypeError("Year must be an integer")

        # Take out world or not for plot
        if not isinstance(with_world, bool):
            raise TypeError("with_world must be a boolean")

        # In case dataframe did not find selected country show an ouptut message
        if year not in self.unique_values(index=True).year:
            raise ValueError(f"The year {year} has no data in the dataframe")

        if with_world:
            world_df = self.df_energy.copy()
        else:
            world_df = self.df_energy[self.df_energy["country"] != "World"].copy()

        # Filter on year
        year_df = world_df[world_df.index.year == year].copy()

        # Create columns for plot
        # Population_mil is created for better visibility of the population in plot
        year_df["total_energy_consumption"] = (
            year_df.loc[:, self.consumption_cols].sum(axis=1).round(3)
        )
        year_df["population_mil"] = year_df["population"] / 1000000

        # Get selection of columns for interactive legend of chart
        selection = alt.selection_multi(fields=["country"], bind="legend")

        # Creation of chart with altair
        chart = (
            alt.Chart(year_df)
            .mark_circle()
            .encode(
                x=alt.X(
                    "gdp:Q",
                    axis=alt.Axis(title="GDP", labelExpr="'$' + datum.value/1E9 + 'B'"),
                    scale=alt.Scale(type="symlog"),
                ),
                y=alt.Y(
                    "total_energy_consumption:Q",
                    axis=alt.Axis(title="Total Energy Consumption"),
                    scale=alt.Scale(type="symlog"),
                ),
                color="country:N",
                size=alt.Size("population_mil"),
                opacity=alt.condition(
                    selection, alt.value(1), alt.value(0.2)
                ),  # Changes opacity if source is selected in legend
                tooltip=[
                    alt.Tooltip(title="Country", field="country"),
                    alt.Tooltip(
                        title="Total Energy Consumption",
                        field="total_energy_consumption",
                    ),
                    alt.Tooltip(title="GDP", field="gdp"),
                ],
            )
            .properties(title=f"Gapminder plot of {year}", width=800, height=300)
            .add_selection(selection)
        )

        return chart

    def gapminder_emissions(
        self, year: int, with_world: bool = True
    ) -> alt.vegalite.v4.api.Chart:
        """
        Plots the emissions and total energy consumption of all countries for a specific year

        Parameters
        ---------------
        year: int
            The year to plot the chart
        with_world: bool
            Have the world as a country in the dataframe or not

        Returns
        ---------------
        chart: alt.vegalite.v4.api.Chart
            Scatter plot of all countries with the emissions as the x-axis
            and the total energy consumption as the y-axis.
            The sizes are based on the population size.
        """

        # In case someone enters something else than a string or a list
        if not isinstance(year, int):
            raise TypeError("Year must be an integer")

        # Take out world or not for plot
        if not isinstance(with_world, bool):
            raise TypeError("with_world must be a boolean")

        # In case dataframe did not find selected country show an ouptut message
        if year not in self.unique_values(index=True).year:
            raise ValueError(f"The year {year} has no data in the dataframe")

        if with_world:
            world_df = self.df_energy.copy()
        else:
            world_df = self.df_energy[self.df_energy["country"] != "World"].copy()

        # Filter on year
        year_df = world_df[world_df.index.year == year].copy()

        # Create columns for plot
        # Population_mil is created for better visibility of the population in plot
        year_df["total_energy_consumption"] = (
            year_df.loc[:, self.consumption_cols].sum(axis=1).round(3)
        )
        year_df["population_mil"] = year_df["population"] / 1000000

        # Get selection of columns for interactive legend of chart
        selection = alt.selection_multi(fields=["country"], bind="legend")

        # Creation of chart with altair
        chart = (
            alt.Chart(year_df)
            .mark_circle()
            .encode(
                x=alt.X(
                    "emissions:Q",
                    axis=alt.Axis(title="Emissions"),
                    scale=alt.Scale(type="symlog"),
                ),
                y=alt.Y(
                    "total_energy_consumption:Q",
                    axis=alt.Axis(title="Total Energy Consumption"),
                    scale=alt.Scale(type="symlog"),
                ),
                color="country:N",
                size=alt.Size("population_mil"),
                opacity=alt.condition(
                    selection, alt.value(1), alt.value(0.2)
                ),  # Changes opacity if source is selected in legend
                tooltip=[
                    alt.Tooltip(title="Country", field="country"),
                    alt.Tooltip(
                        title="Total Energy Consumption",
                        field="total_energy_consumption",
                    ),
                    alt.Tooltip(title="Emissions", field="emissions"),
                ],
            )
            .properties(
                title=f"Emissions-Energy Consumption plot of {year}",
                width=800,
                height=300,
            )
            .add_selection(selection)
        )

        return chart

    def arima(self, country: str, ad_points: int) -> alt.vegalite.v4.api.HConcatChart:
        """
        Returns two plots.

        One for an ARIMA prediction on total energy consumption and another for an ARIMA prediction
        for total emissions.

        Parameters
        ---------------
        country: string
            Name of the country to use when plotting.

        ad_points: int
            Number of predicted years.

        Returns
        ---------------
        final_chart: alt.vegalite.v4.api.HConcatChart
            Two line charts side by side, each with current data plus predicted by the arima model.
            Charts with information regarding total energy consumption
            and total emissions regarding selected country.

        """
        if country not in self.unique_values():
            raise ValueError(f"Country {country} is not in the data set")

        if not isinstance(ad_points, int):
            raise TypeError("Only integers are allowed")

        if ad_points < 1:
            raise ValueError("Number of years can not be less than 1")

        arima_df = self.df_energy.copy()
        arima_df["total_consumption"] = (
            arima_df[self.consumption_cols].sum(axis=1).round(3)
        )

        # model consumption
        data_consumption = arima_df[arima_df["country"] == country]["total_consumption"]

        model = ARIMA(data_consumption, order=(3, 1, 0))
        model_fit = model.fit()

        yhat_cons = model_fit.predict(len(data_consumption),
                                      len(data_consumption) + ad_points - 1,
                                      typ="levels").round(3)

        # plot
        chart_cons = (
            alt.Chart(data_consumption.reset_index())
            .mark_line()
            .encode(
                x=alt.X("year:T", axis=alt.Axis(title="Year")),
                y=alt.Y(
                    "total_consumption:Q", axis=alt.Axis(title="Total consumption")
                ),
                tooltip=[
                    alt.Tooltip(title="Total consumption", field="total_consumption"),
                ],
            )
            .properties(
                title=f"Total energy consumption for {country}", width=400, height=300
            )
        )

        pred_cons = (
            alt.Chart(yhat_cons.reset_index())
            .mark_line(color="green")
            .encode(
                x=alt.X("index:T", axis=alt.Axis(title="Year")),
                y=alt.Y(
                    "predicted_mean:Q",
                    axis=alt.Axis(
                        title="Total consumption", labelExpr="datum.value + 'TWh'"
                    ),
                ),
                tooltip=[
                    alt.Tooltip(title="Total consumption", field="predicted_mean"),
                ],
            )
            .properties(
                title=f"Total energy consumption for {country}", width=400, height=300
            )
        )

        # -------------------------------------------------------
        # model emissions
        data_emissions = arima_df[arima_df["country"] == country]["emissions"].copy()

        model = ARIMA(data_emissions, order=(3, 1, 0))
        model_fit = model.fit()

        yhat_ems = model_fit.predict(len(data_emissions),
                                     len(data_emissions) + ad_points - 1,
                                     typ="levels").round(3)

        # ------------------------------------------
        chart_ems = (
            alt.Chart(data_emissions.reset_index())
            .mark_line()
            .encode(
                x=alt.X("year:T", axis=alt.Axis(title="Year")),
                y=alt.Y(
                    "emissions:Q",
                    axis=alt.Axis(
                        title="Total emissions", labelExpr="datum.value + ' t/TWh'"
                    ),
                ),
                tooltip=[
                    alt.Tooltip(title="Total emissions", field="emissions"),
                ],
            )
            .properties(title=f"Emissions for {country}", width=400, height=300)
        )

        pred_ems = (
            alt.Chart(yhat_ems.reset_index())
            .mark_line(color="red")
            .encode(
                x=alt.X("index:T", axis=alt.Axis(title="Year")),
                y=alt.Y("predicted_mean:Q", axis=alt.Axis(title="Total emissions")),
                tooltip=[
                    alt.Tooltip(title="Total emissions", field="predicted_mean"),
                ],
            )
            .properties(title=f"Emissions for {country}", width=400, height=300)
        )

        return chart_cons + pred_cons | chart_ems + pred_ems
