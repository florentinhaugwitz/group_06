# Advanced Programming for Data Science - Group 06

This project consists of an analyis of the worldwide energy consumption since 1970. The goal is to display the data in various charts and to filter on individual countries or show a comparison between countries. The final outcome of the project can be seen in the [showcase_notebook.ipynb](showcase_notebook.ipynb) file.


## Installation
To start the project please create the same environment with the use of the adpro.yml file
```bash
$ conda env create --file adpro.yml
```

## Data Source
The used data set can be found here: https://raw.githubusercontent.com/owid/energy-data/master/owid-energy-data.csv
Alternatively, a different dataset can be used when initallizing the object of the ErgoAnalysis


## Structure
The downloads folder contains the downloaded data file in a csv format.
The prototype_notebooks folder contains Jupyter Notebooks for building and testing the ergo_analysis.py file


## Authors 
- Diogo Passeiro (39339@novasbe.pt)
- Tomás Mendes (48429@novasbe.pt)
- Rodrigo Andrade (50982@novasbe.pt)
- Florentin von Haugwitz (48174@novasbe.pt)


## License
This Project is equipped with a MIT License. Please refer to this [file](LICENSE) for further details.
